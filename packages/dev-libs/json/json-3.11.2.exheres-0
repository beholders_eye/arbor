# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=nlohmann tag=v${PV} ] cmake

SUMMARY="JSON for Modern C++"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

_TEST_DATA_VER="3.1.0"

DEPENDENCIES=""

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DNLOHMANN_JSON_CONFIG_INSTALL_DIR="/usr/$(exhost --target)/lib/cmake/nlohmann_${PN}"
    -DJSON_CI:BOOL=FALSE
    -DJSON_MultipleHeaders:BOOL=TRUE
    -DJSON_Install:BOOL=TRUE
    -DJSON_TestDataDirectory:PATH="${WORKBASE}"/${PN}_test_data-${_TEST_DATA_VER}
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    '-DJSON_BuildTests:BOOL=TRUE -DJSON_BuildTests:BOOL=FALSE'
    -DJSON_Valgrind:BOOL=FALSE
)
# Skip tests of the cmake fetching stuff, which we don't run and can't
# use anyway
DEFAULT_SRC_TEST_PARAMS=( ARGS+="-E cmake_fetch_content" )

src_fetch_extra() {
    if expecting_tests && [[ ! -e "${FETCHEDDIR}"/${PN}_test_data-${_TEST_DATA_VER}.tar.xz ]] ; then
        edo wget -P "${FETCHEDDIR}" https://dev.exherbo.org/distfiles/${PN}/${PN}_test_data-${_TEST_DATA_VER}.tar.xz
    fi
}

src_unpack() {
    cmake_src_unpack

    if expecting_tests ; then
        edo tar xJf "${FETCHEDDIR}"/${PN}_test_data-${_TEST_DATA_VER}.tar.xz
    fi
}

src_install() {
    cmake_src_install

    # remove additional lib{32,64} appearing when tests are enabled
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib{32,64}
}

