# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require meson

SUMMARY="A simple network library"
HOMEPAGE="https://www.gnome.org/"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    gnome-proxy [[
        description = [ support for GNOME proxy configuration ]
        requires = [ libproxy ]
    ]]
    libproxy
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.4]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.73.3]
        gnome-proxy? ( gnome-desktop/gsettings-desktop-schemas )
        libproxy? ( net-libs/libproxy:1[>=0.4.16] )
        providers:gnutls? ( dev-libs/gnutls[>=3.7.4] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.0.2] )
    test:
        providers:gnutls? ( dev-libs/gnutls[>=3.6.5][pkcs11] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/meson-install-glib-pacrunner.service-into-libdir.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddebug_logs=false
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gnome-proxy gnome_proxy'
    libproxy
    '!libproxy environment_proxy'
    'providers:gnutls gnutls'
    '!providers:gnutls openssl'
)

src_prepare() {
    meson_src_prepare

    # do not build the tls tests unconditionally since they require
    # gnutls[pkcs11] which the actual code does not yet.
    # last checked: 2.68.1
    ! expecting_tests && edo sed \
        -e "/^subdir('tests')/d" \
        -i tls/meson.build
}

