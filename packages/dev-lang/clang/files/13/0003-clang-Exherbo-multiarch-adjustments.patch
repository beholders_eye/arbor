From 995a0f40707ba601590aba618e587f4d3460fe20 Mon Sep 17 00:00:00 2001
From: Marvin Schmidt <marv@exherbo.org>
Date: Fri, 29 Oct 2021 13:52:35 +0200
Subject: [PATCH 3/5] clang: Exherbo multiarch adjustments

---
 clang/lib/Driver/ToolChains/Gnu.cpp   | 12 ++++++++
 clang/lib/Driver/ToolChains/Linux.cpp | 42 +++++++++++++++++++++++----
 2 files changed, 49 insertions(+), 5 deletions(-)

diff --git a/clang/lib/Driver/ToolChains/Gnu.cpp b/clang/lib/Driver/ToolChains/Gnu.cpp
index da39f29e4619..f8cc7c03d550 100644
--- a/clang/lib/Driver/ToolChains/Gnu.cpp
+++ b/clang/lib/Driver/ToolChains/Gnu.cpp
@@ -17,6 +17,7 @@
 #include "Linux.h"
 #include "clang/Config/config.h" // for GCC_INSTALL_PREFIX
 #include "clang/Driver/Compilation.h"
+#include "clang/Driver/Distro.h"
 #include "clang/Driver/Driver.h"
 #include "clang/Driver/DriverDiagnostic.h"
 #include "clang/Driver/Options.h"
@@ -2797,6 +2798,17 @@ void Generic_GCC::AddMultilibPaths(const Driver &D,
     const llvm::Triple &GCCTriple = GCCInstallation.getTriple();
     const std::string &LibPath =
         std::string(GCCInstallation.getParentLibPath());
+    const Distro Distro(D.getVFS(), GCCTriple);
+
+    // On Exherbo, the GCC installation will reside in e.g.
+    //   /usr/x86_64-pc-linux-gnu/lib/gcc/armv7-unknown-linux-gnueabihf/9.2.0
+    // while the matching lib path is
+    //   /usr/armv7-unknown-linux-gnueabihf/lib
+    if (Distro == Distro::Exherbo)
+      addPathIfExists(D,
+                      LibPath + "/../../" + GCCTriple.str() + "/lib/../" +
+                          OSLibDir + SelectedMultilib.osSuffix(),
+                      Paths);
 
     // Sourcery CodeBench MIPS toolchain holds some libraries under
     // a biarch-like suffix of the GCC installation.
diff --git a/clang/lib/Driver/ToolChains/Linux.cpp b/clang/lib/Driver/ToolChains/Linux.cpp
index c9360fc67165..f9db2c00603a 100644
--- a/clang/lib/Driver/ToolChains/Linux.cpp
+++ b/clang/lib/Driver/ToolChains/Linux.cpp
@@ -550,6 +550,17 @@ void Linux::AddClangSystemIncludeArgs(const ArgList &DriverArgs,
   if (DriverArgs.hasArg(options::OPT_nostdlibinc))
     return;
 
+  // Exherbo's multiarch layout is /usr/<triple>/include and not
+  // /usr/include/<triple>
+  const Distro Distro(D.getVFS(), getTriple());
+  if (Distro == Distro::Exherbo) {
+    std::string MultiarchIncludeDir = "/usr/" + getTriple().str() + "/include";
+
+    if (D.getVFS().exists(SysRoot + MultiarchIncludeDir))
+      addSystemInclude(DriverArgs, CC1Args,
+                       SysRoot + MultiarchIncludeDir);
+  }
+
   // LOCAL_INCLUDE_DIR
   addSystemInclude(DriverArgs, CC1Args, SysRoot + "/usr/local/include");
   // TOOL_INCLUDE_DIR
@@ -597,8 +608,33 @@ void Linux::addLibStdCxxIncludePaths(const llvm::opt::ArgList &DriverArgs,
   if (!GCCInstallation.isValid())
     return;
 
-  // Detect Debian g++-multiarch-incdir.diff.
+  const Driver &D = getDriver();
+  const Distro Distro(D.getVFS(), getTriple());
+  StringRef LibDir = GCCInstallation.getParentLibPath();
+  const Multilib &Multilib = GCCInstallation.getMultilib();
+  const GCCVersion &Version = GCCInstallation.getVersion();
   StringRef TripleStr = GCCInstallation.getTriple().str();
+
+  // Handle Exherbo before Debian because the logic in
+  // Generic_GCC::addGCCLibStdCxxIncludePaths would add
+  //   <LibDir>/../include/c++/<version>
+  // which on Exherbo result in
+  //   LibDir = /usr/host/lib/gcc/<triple>/<version>/../../..
+  //          = /usr/host/lib/
+  //   => /usr/host/lib/../include/c++/<version>
+  //   => /usr/host/include/c++/<version>
+  //
+  // addLibStdCXXIncludePaths would then check if "/usr/host/include/c++/<version>"
+  // exists and add that as include path, while we need
+  //   /usr/<triple>/include/c++/<version>
+  // for cross-compilation to work
+  if (Distro == Distro::Exherbo)
+    if (addLibStdCXXIncludePaths(LibDir.str() + "/../../" + TripleStr +
+                                     "/include/c++/" + Version.Text, TripleStr,
+                                 Multilib.includeSuffix(), DriverArgs, CC1Args))
+      return;
+
+  // Detect Debian g++-multiarch-incdir.diff.
   StringRef DebianMultiarch =
       GCCInstallation.getTriple().getArch() == llvm::Triple::x86
           ? "i386-linux-gnu"
@@ -609,10 +645,6 @@ void Linux::addLibStdCxxIncludePaths(const llvm::opt::ArgList &DriverArgs,
                                                DebianMultiarch))
     return;
 
-  StringRef LibDir = GCCInstallation.getParentLibPath();
-  const Multilib &Multilib = GCCInstallation.getMultilib();
-  const GCCVersion &Version = GCCInstallation.getVersion();
-
   const std::string LibStdCXXIncludePathCandidates[] = {
       // Android standalone toolchain has C++ headers in yet another place.
       LibDir.str() + "/../" + TripleStr.str() + "/include/c++/" + Version.Text,
-- 
2.33.1

