# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2019 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project

SUMMARY="A next generation, high-performance debugger"

MYOPTIONS="
    doc
    libedit
    python
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            dev-python/epydoc[python_abis:*(-)?]
            media-gfx/graphviz
        )
    build+run:
        dev-lang/clang:*[~${PV}]
        dev-lang/llvm:*[~${PV}]
        dev-libs/libxml2:2.0
        dev-python/six[python_abis:*(-)?]
        libedit? ( dev-libs/libedit )
        python? ( dev-lang/swig[>=3.0] )
"

# Fails a lot of tests, upstream is aware of it being broken
# http://lists.llvm.org/pipermail/llvm-dev/2019-September/135114.html
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DClang_DIR:STRING="${LLVM_PREFIX}"/lib/cmake/clang

    -DLLDB_ENABLE_CURSES:BOOL=ON
    -DLLDB_USE_SYSTEM_SIX:BOOL=ON
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'doc LLDB_BUILD_DOCUMENTATION'
    'libedit LLDB_ENABLE_LIBEDIT'
    'python LLDB_ENABLE_PYTHON'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DLLDB_INCLUDE_TESTS:BOOL=TRUE -DLLDB_INCLUDE_TESTS:BOOL=FALSE'
)
