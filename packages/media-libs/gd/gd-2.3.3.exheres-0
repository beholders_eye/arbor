# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gd-2.0.35.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

MY_PN="lib${PN}"

require github [ pn=${MY_PN} release=${PNV} suffix=tar.xz ]

SUMMARY="Graphics library for fast image creation"
HOMEPAGE="https://${MY_PN}.github.io"

LICENCES="as-is"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    avif [[ description = [ Support for the AV1 Image File Format ] ]]
    fontconfig [[ description = [ Support for managing custom fonts via fontconfig ] ]]
    heif [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    tiff
    truetype
    webp
    xpm
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# https://github.com/libgd/libgd/issues/217
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libpng:=
        sys-libs/zlib
        avif? ( media-libs/libavif:=[>=0.8.2] )
        fontconfig? ( media-libs/fontconfig )
        heif? ( media-libs/libheif[>=1.7.0] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff )
        truetype? ( media-libs/freetype:2[>=2.1.10] )
        webp? ( media-libs/libwebp:=[>=0.2.0] )
        xpm? (
            x11-libs/libXpm
            x11-libs/libXt
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-gd-formats
    --disable-static
    --disable-werror
    --with-jpeg
    --with-png
    --with-zlib
    --without-liq
    --without-raqm
    --without-x
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    avif
    fontconfig
    heif
    tiff
    "truetype freetype"
    webp
    xpm
)

