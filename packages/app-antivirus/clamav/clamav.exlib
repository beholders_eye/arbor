# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2020 Ali Polatel <alip@exherbo.org>
# Copyright 2020 Maxime Sorin <maxime.sorin@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'clamav-0.93.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require cmake systemd-service

export_exlib_phases src_prepare src_install src_test pkg_postinst

SUMMARY="Clam Anti-Virus Scanner"
HOMEPAGE="https://www.${PN}.net"
DOWNLOADS="${HOMEPAGE}/downloads/production/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="https://github.com/vrtadmin/${PN}-devel/blob/master/README"
UPSTREAM_DOCUMENTATION="
    ${HOMEPAGE}/documents [[ lang = en description = [ Official Documentation ] ]]
    https://github.com/vrtadmin/${PN}-faq [[ lang = en description = [ Official FAQs ] ]]
"

LICENCES="
    Apache-2.0 [[ note = [ YARA ] ]]
    BSD-3
    || ( GPL-2 ( GPL-2 unRAR ) [ [[ note = [ libclamunrar uses code under unRAR licence ] ]] )
"
SLOT="0"
MYOPTIONS="
    systemd
    milter [[ description = [ antivirus scanner using mail-filter/libmilter ] ]]
    unrar [[ description = [ build libclamunrar to enable scanning rar archives ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/rust:*[>=1.56]
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        dev-libs/gmp:= [[ note = [ optional; for digital signatures ] ]]
        dev-libs/json-c:= [[ note = [ optional; for file property meta data collecting ] ]]
        dev-libs/libxml2:2.0 [[ note = [ optional; for DMG and XAR support ] ]]
        dev-libs/pcre2[>=10.30] [[ note = [ prioritizes PCRE2 over PCRE ] ]]
        net-misc/curl[>=7.45] [[ note = [ clamonacc and freshclam dependency ] ]]
        systemd? ( sys-apps/systemd )
        sys-libs/ncurses [[ note = [ optional; for clamdtop ] ]]
        sys-libs/zlib
        milter? (
            mail-filter/libmilter
            net-misc/curl [[ note = [ for following mail URLs ] ]]
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        group/${PN}
        user/${PN}
    test:
        dev-lang/python:*[>=3]
        dev-libs/check
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DAPP_CONFIG_DIRECTORY:STRING=/etc
    -DBYTECODE_RUNTIME:STRING=interpreter
    -DCLAMAV_GROUP:STRING=clamav
    -DCLAMAV_USER:STRING=clamav
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Valgrind:BOOL=TRUE
    -DCMAKE_INSTALL_SBINDIR:PATH=bin
    -DDATABASE_DIRECTORY:STRING=/var/lib/${PN}
    -DENABLE_ALL_THE_WARNINGS:BOOL=FALSE
    -DENABLE_CLAMONACC:BOOL=TRUE
    -DENABLE_DEBUG:BOOL=FALSE
    -DENABLE_DOXYGEN:BOOL=FALSE
    -DENABLE_EXAMPLES:BOOL=FALSE
    -DENABLE_EXPERIMENTAL:BOOL=FALSE
    -DENABLE_EXTERNAL_MSPACK:BOOL=FALSE
    -DENABLE_EXTERNAL_TOMSFASTMATH:BOOL=FALSE
    -DENABLE_FRESHCLAM_DNS_FIX:BOOL=TRUE
    -DENABLE_FRESHCLAM_NO_CACHE:BOOL=FALSE
    -DENABLE_FUZZ:BOOL=FALSE
    -DENABLE_JSON_SHARED:BOOL=TRUE
    -DENABLE_LIBCLAMAV_ONLY:BOOL=FALSE
    -DENABLE_MAN_PAGES:BOOL=TRUE
    -DENABLE_SHARED_LIB:BOOL=TRUE
    -DENABLE_STATIC_LIB:BOOL=FALSE
    -DENABLE_STRN_INTERNAL:BOOL=FALSE
    -DENABLE_WERROR:BOOL=FALSE
    -DDISABLE_MPOOL:STRING=0
    -DOPTIMIZE:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'milter MILTER'
    'systemd SYSTEMD'
    'unrar UNRAR'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTS:BOOL=TRUE -DENABLE_TESTS:BOOL=FALSE'
)

clamav_src_prepare() {
    cmake_src_prepare

    edo sed \
        -e 's:/root/quarantine:/var/lib/clamav/quarantine:g' \
        -i clamonacc/clamav-clamonacc.service.in
}

clamav_src_test() {
    esandbox allow_net "unix:${WORK}/unit_tests/*/clamd-test.socket"
    esandbox allow_net --connect "unix:${WORK}/unit_tests/*/clamd-test.socket"
    esandbox allow_net --connect "inet:127.0.0.1@3319"
    # freshclam
    #esandbox allow_net --connect "LOCAL@53"
    #esandbox allow_net --bind "inet:0.0.0.0@8001"

    default

    #esandbox disallow_net --bind "inet:0.0.0.0@8001"
    #esandbox disallow_net --connect "LOCAL@53"
    esandbox disallow_net --connect "inet:127.0.0.1@3319"
    esandbox disallow_net --connect "unix:${WORK}/unit_tests/*/clamd-test.socket"
    esandbox disallow_net "unix:${WORK}/unit_tests/*/clamd-test.socket"
}

clamav_src_install() {
    cmake_src_install

    insinto "${SYSTEMDTMPFILESDIR}"
    hereins ${PN}.conf <<EOF
d /run/${PN} 0755 ${PN} ${PN}
EOF

    keepdirs=(
        /var/lib/${PN}
        /var/lib/${PN}/quarantine
        /usr/${HOST}/share/${PN}
    )
    for d in ${keepdirs[@]}; do
        keepdir $d
    done

    # Set up some default configs
    edo sed -e "s:^\(Example\):\# \1:" \
            -e "s:.*\(PidFile\) .*:\1 /run/${PN}/clamd.pid:" \
            -e "s:.*\(LocalSocket\) .*:\1 /run/${PN}/clamd.ctl:" \
            -e "s:.*\(User\) .*:\1 ${PN}:" \
            -e "s:^\#\(LogFile\) .*:\1 /var/log/${PN}/clamd.log:" \
            -e "s:^\#\(LogTime\).*:\1 yes:" \
            -e "s:^\#\(AllowSupplementaryGroups\).*:\1 yes:" \
            -e "s:^\#\(DatabaseDirectory\).*:\1 /var/lib/${PN}:" \
            -e '/OnAccessIncludePath \/home/s/^#//g' \
            -e "s:.*\(OnAccessExcludeUname\) .*:\1 ${PN}:" \
            "${IMAGE}"/etc/clamd.conf.sample > "${IMAGE}"/etc/clamd.conf

    edo sed -e "s:^\(Example\):\# \1:" \
            -e "s:.*\(PidFile\) .*:\1 /run/${PN}/freshclam.pid:" \
            -e "s:.*\(DatabaseOwner\) .*:\1 ${PN}:" \
            -e "s:^\#\(UpdateLogFile\) .*:\1 /var/log/${PN}/freshclam.log:" \
            -e "s:^\#\(NotifyClamd\).*:\1 /etc/clamd.conf:" \
            -e "s:^\#\(ScriptedUpdates\).*:\1 yes:" \
            -e "s:^\#\(AllowSupplementaryGroups\).*:\1 yes:" \
            -e "s:^\#\(DatabaseDirectory\).*:\1 /var/lib/${PN}:" \
            "${IMAGE}"/etc/freshclam.conf.sample > "${IMAGE}"/etc/freshclam.conf

    if option milter; then
        edo sed -e "s:^\(Example\):\# \1:" \
            -e "s:.*\(PidFile\) .*:\1 /run/${PN}/${PN}-milter.pid:" \
            -e "s+^\#\(ClamdSocket\) .*+\1 unix:/run/${PN}/clamd.sock+" \
            -e "s:.*\(User\) .*:\1 ${PN}:" \
            -e "s+^\#\(MilterSocket\) /.*+\1 unix:/run/${PN}/${PN}-milter.sock+" \
            -e "s:^\#\(AllowSupplementaryGroups\).*:\1 yes:" \
            -e "s:^\#\(LogFile\) .*:\1 /var/log/${PN}/${PN}-milter.log:" \
            "${IMAGE}"/etc/${PN}-milter.conf.sample > "${IMAGE}"/etc/${PN}-milter.conf
    fi
}

clamav_pkg_postinst() {
    edo chown -R ${PN}:${PN} /var/lib/${PN}
    edo chown -R ${PN}:${PN} /usr/${HOST}/share/${PN}
    edo chmod g+w /var/lib/${PN}

    if [[ ! -f /var/log/${PN}/freshclam.log ]]; then
        edo mkdir -p /var/log/${PN}
        edo touch /var/log/${PN}/freshclam.log
        edo chown -R ${PN}:${PN} /var/log/${PN}
        edo chmod g+w /var/log/${PN}/freshclam.log
    fi

    einfo "You must run freshclam manually or start the clamav-freshclam service to populate"
    einfo "the virus database before starting clamav."
    einfo "If you get \"No space left on device\" from the On-Access Scanner you may need to"
    einfo "raise fs.inotify.max_user_watches."
}

