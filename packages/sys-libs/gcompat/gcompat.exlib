# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix='https://code.foxkit.us' user='adelie' ]

export_exlib_phases src_compile src_install

SUMMARY="The GNU C Library Compatibility Layer."

LICENCES="UoI-NCSA"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libucontext
        dev-libs/musl-obstack
    run:
        !sys-libs/glibc [[
            description = [ gcompat is a glibc compatibility layer for musl ]
            resolution = manual
        ]]
"

get_ld_name() {
    case $(exhost --target) in
        x86_64*)
            echo ld-linux-x86-64.so.2
            ;;
        *)
            die "$(exhost --target) is not supported"
            ;;
    esac
}

get_musl_ld() {
    case $(exhost --target) in
        x86_64*)
            echo /usr/$(exhost --target)/lib/ld-musl-x86_64.so.1
            ;;
        *)
            die "$(exhost --target) is not supported"
            ;;
    esac
}

gcompat_get_args() {
    local args=(
        PKG_CONFIG=$(exhost --tool-prefix)pkg-config
        LIBGCOMPAT_PATH=/usr/$(exhost --target)/lib/lib${PN}.so.0
        LINKER_PATH=$(get_musl_ld)
        LOADER_NAME=$(get_ld_name)
        LOADER_PATH=/usr/$(exhost --target)/lib/$(get_ld_name)
        WITH_LIBUCONTEXT=yes
    )

    echo "${args[@]}"
}

gcompat_src_compile() {
    emake $(gcompat_get_args)
}

gcompat_src_install() {
    emake install $(gcompat_get_args) DESTDIR="${IMAGE}"

    emagicdocs
}


